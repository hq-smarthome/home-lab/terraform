# Allow Game Servers to read and list any secrets it has
path "game-servers/*" {
  capabilities = ["read", "list"]
}
