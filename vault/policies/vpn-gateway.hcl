# Allow VPN gateway to read and list any secrets it has
path "vpn-gateway/*" {
  capabilities = ["read", "list"]
}
