# Allow lets encrypt to read and list any secrets it has
path "lets-encrypt/*" {
  capabilities = ["read", "list"]
}
