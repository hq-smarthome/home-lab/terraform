# Allow NZBGet to read and list any secrets it has
path "nzbget/*" {
  capabilities = ["read", "list"]
}
