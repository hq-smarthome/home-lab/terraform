# Allow home assistant to read and list any secrets it has
path "home-assistant/*" {
  capabilities = ["read", "list"]
}
