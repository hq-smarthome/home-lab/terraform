# Allow DDNS to read and list any secrets it has
path "dynamic-dns/*" {
  capabilities = ["read", "list"]
}
