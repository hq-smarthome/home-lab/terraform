# Allow pi hole to read any secrets it has
path "pi-hole/*" {
  capabilities = ["read"] 
}
