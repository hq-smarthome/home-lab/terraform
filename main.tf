terraform {
  required_version = ">= 0.14"

  backend "consul" {
    path = "terraform"
    datacenter = "proxima"
  }
}

provider "vault" {}

# Base vault policies
resource "vault_policy" "default" {
  name   = "default"
  policy = file("vault/policies/default.hcl")
}

resource "vault_policy" "nomad-server" {
  name   = "nomad-server"
  policy = file("vault/policies/nomad-server.hcl")
}

resource "vault_policy" "home-assistant" {
  name   = "home-assistant"
  policy = file("vault/policies/home-assistant.hcl")
}

resource "vault_policy" "pi-hole" {
  name   = "pi-hole"
  policy = file("vault/policies/pi-hole.hcl")
}

resource "vault_policy" "lets-encrypt" {
  name   = "lets-encrypt"
  policy = file("vault/policies/lets-encrypt.hcl")
}

resource "vault_policy" "vpn-gateway" {
  name   = "vpn-gateway"
  policy = file("vault/policies/vpn-gateway.hcl")
}

resource "vault_policy" "nzbget" {
  name   = "nzbget"
  policy = file("vault/policies/nzbget.hcl")
}

resource "vault_policy" "game-servers" {
  name   = "game-servers"
  policy = file("vault/policies/game-servers.hcl")
}

resource "vault_policy" "dynamic-dns" {
  name   = "dynamic-dns"
  policy = file("vault/policies/dynamic-dns.hcl")
}

resource "vault_policy" "snipe-it" {
  name   = "snipe-it"
  policy = file("vault/policies/snipe-it.hcl")
}

resource "vault_policy" "database-sql" {
  name   = "database-sql"
  policy = file("vault/policies/database-sql.hcl")
}

resource "vault_policy" "database-ts" {
  name   = "database-ts"
  policy = file("vault/policies/database-ts.hcl")
}

resource "vault_policy" "traefik-pilot" {
  name   = "traefik-pilot"
  policy = file("vault/policies/traefik-pilot.hcl")
}

resource "vault_policy" "mqtt-management" {
  name   = "mqtt-management"
  policy = file("vault/policies/mqtt-management.hcl")
}

resource "vault_policy" "identity-access-management" {
  name   = "identity-access-management"
  policy = file("vault/policies/identity-access-management.hcl")
}


# vault roles
resource "vault_auth_backend" "nomad-cluster" {
  type = "approle"
  path = "nomad-cluster"
}

resource "vault_token_auth_backend_role" "nomad-cluster" {
  role_name = "nomad-cluster"
  allowed_policies = [
    "lets-encrypt",
    "pi-hole",
    "home-assistant",
    "vpn-gateway",
    "nzbget",
    "game-servers",
    "dynamic-dns",
    "database-sql",
    "database-ts",
    "traefik-pilot",
    "snipe-it",
    "mqtt-management",
    "identity-access-management"
  ]
  disallowed_policies = ["nomad-server"]
  orphan = true
  renewable = true
  token_period = "86400"
  // token_explicit_max_ttl = "115200"
}
