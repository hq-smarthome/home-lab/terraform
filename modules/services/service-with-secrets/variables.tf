variable "service-name" {
  description = "The name of the service, this will be used for fault paths"
  type = string
}
