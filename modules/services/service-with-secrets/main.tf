terraform {
  required_version = ">= 0.14"
}

resource "vault_policy" "${var.service-name}" {
  name = "${var.service-name}"
  policy = <<-EOT
  path "hq/services/${var.service-name}" {
    capabilities = ["read", "list"]
  }
  EOT
}
